var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
// var db = mongojs('mongodb://test:test@ds119588.mlab.com:19588/booking_rooms_cba', ['Rooms']);
var db = mongojs('mongodb://test:test@ds119588.mlab.com:19588/booking_rooms_cba', ['roomsTestEmil']);

//Get all rooms with a null value in users #AllAvailable
router.get('/rooms',  function (req, res, next) {
    db.roomsTestEmil.find({times:[]},function (err, rooms) {
        if (err) {
            res.send(err);
        } else {
            res.json(rooms)
        }
    });
});

/* GET One Todo with the provided ID */
// router.get('/rooms/:id', function (req, res, next) {
//     db.Rooms.findOne({
//         _id: mongojs.ObjectId(req.params.id)
//     }, function (err, Rooms) {
//         if (err) {
//             res.send(err);
//         } else {
//             console.log(Rooms);
//             res.json(Rooms);
//         }
//     });
// });

//Test for changing user: null to user: "Emil"
//To be done. Ændrer til et navn der er inputtet.
router.get('/rooms/:id/:name', function (req, res, next) {
    db.roomsTestEmil.findAndModify({
        query: {_id: mongojs.ObjectId(req.params.id)},
        update: {
            $push: {
                times: {
                    "date": new Date().getDate()+"/"+(new Date().getMonth()+1) + "/" + new Date().getFullYear(),
                    "hour": new Date().getHours(),
                    "hour2": (new Date().getHours()+1),
                    "user": req.params.name
                }
            }
        }, //insert data.
        new: true
    }, function (err, Rooms) {
        if (err) {
            console.log(err);
        }
    });
});
module.exports = router;