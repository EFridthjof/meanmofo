import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class RoomsService {

  constructor(private http: Http) { }

  // Get all Rooms from the API
  getAllRooms() {
    return this.http.get('/api/rooms')
      .map(res => res.json());
  }

  getOneRoom(id, name){
    {
      return this.http.get('/api/rooms/'+id+'/'+name)
          .map(res => res.json());
    }
  }
}
