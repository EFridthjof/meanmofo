import { Component, OnInit } from '@angular/core';
import { RoomsService } from '../rooms.service';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {IMyOptions} from 'mydatepicker';


@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {
  // instantiate rooms to an empty object
  rooms: any = [];

  constructor(private roomsService: RoomsService,private formBuilder: FormBuilder) { }

  ngOnInit() {
    // Retrieve rooms from the API
    this.roomsService.getAllRooms().subscribe(rooms => {
      this.rooms = rooms;
    });
    this.myForm = this.formBuilder.group({
      //myDate: [null, Validators.required]   // not initial date set
      //myDate: [{jsdate: new Date()}, Validators.required] // initialize today with jsdate property
      myDate: [{date: {year: 2018, month: 10, day: 9}}, Validators.required]   // this example is initialized to specific date
    });
  }

  getOne(room, name){
    this.roomsService.getOneRoom(room._id, name).subscribe(rooms => {
      this.rooms = rooms
    });
  }

  //Datepicker test.
  private myDatePickerOptions: IMyOptions = {
    dateFormat: 'dd.mm.yyyy',
    height: '34px',
    width: '210px',
    inline: false
  };
  // ngModel functions here
  private myForm: FormGroup;
  private model: string = null;   // not initial date set (use null or empty string)
  // private selector: number = 0;

  onSubmitNgModel(): void {
    console.log('Value: ', this.model);
  }

  clearNgModelDate(): void {
    this.model = null;
  }
}